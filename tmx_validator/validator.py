"""
Copyright (C) 2018 Gregory Vigo Torres

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see http://www.gnu.org/licenses/.
"""
import logging
import os
import re

from lxml import etree

from tmx_validator import config


__doc__ = """validate a tmx using a DTD"""

log = logging.getLogger('__name__')


def get_dtd(docinfo):
    """Try to figure out which DTD to use"""
    def ret_dtd(tmx_dtd):
        return etree.DTD(os.path.join(config.DTD_BASEDIR, tmx_dtd))

    try:
        tmx_dtd = docinfo.internalDTD.system_url
        return ret_dtd(tmx_dtd)
    except Exception as E:
        print(E)
        pass

    try:
        tmx_dtd = docinfo.system_url
        return ret_dtd(tmx_dtd)
    except Exception as E:
        print(E)
        pass

    try:
        dt = docinfo.doctype
        tmv = re.search('"([a-z0-9.]+)"', dt)
        tmx_dtd = tmv.group(1)
        return ret_dtd(tmx_dtd)
    except Exception as E:
        print(E)
        pass


def validate_(tmxpath):
    try:
        with open(tmxpath, 'rb') as fd:
            doc = etree.XML(fd.read())
    except Exception as E:
        print(E)
        return False

    root = doc.getroottree()
    docinfo = root.docinfo
    dtd = get_dtd(docinfo)

    if dtd is None:
        log.error('Cannot find DTD or tmx version')
        return

    body = doc.find('body')
    tus = body.iterfind('tu')
    is_valid = True
    for tu in tus:
        val = dtd.validate(tu)
        if not val:
            is_valid = False
            log.error(f'INVALID ELEMENT <{tu.tag}> at line {tu.sourceline}')
            for err in dtd.error_log.filter_from_errors():
                log.error(err)

    return is_valid
