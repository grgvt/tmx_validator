#! /usr/bin/env python3
import click

from tmx_validator import validate_


@click.command()
@click.argument('tmxpath',
                type=click.Path(exists=True,
                                resolve_path=True,
                                dir_okay=False))
def validate(tmxpath):
    is_valid = validate_(tmxpath)
    if is_valid:
        click.echo(f'{tmxpath} is valid')
    else:
        click.echo(f'{tmxpath} is NOT valid')


if __name__ == '__main__':
    validate()
