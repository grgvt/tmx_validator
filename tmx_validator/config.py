import os

LOG_FORMAT = '[%(levelname)s] [%(module)s %(funcName)s %(lineno)d] %(message)s'

DTD_BASEDIR = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'dtd')
