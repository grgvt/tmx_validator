from setuptools import setup, find_packages

VERSION = 1.0

setup(
    name='validatetmx',
    version=VERSION,
    packages=find_packages(),
    install_requires=[
        'click',
        'lxml',
    ],
    entry_points={
        'console_scripts': [
            'validatetmx=tmx_validator.main:validate'
        ]
    }
)
